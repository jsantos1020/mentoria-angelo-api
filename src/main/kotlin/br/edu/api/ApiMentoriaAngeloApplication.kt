package br.edu.api

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ApiMentoriaAngeloApplication

fun main(args: Array<String>) {
    runApplication<ApiMentoriaAngeloApplication>(*args)
}
